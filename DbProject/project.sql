select * from countrytostat;
select * from country;
select * from stat;
select * from hdi.groups;
select * from region;

# Q1
select avg(countrytostat.Value) from countrytostat where StatId = 2;

# Q2
select avg(countrytostat.value) from countrytostat where StatId = 3;

# Q3 also a stored proc.
select c.CountryId, c.CountryName, r.mi as "Men Incomes", r.wi as "Women Incomes", r.d as "Difference" from country as c inner join
(select m.CountryId, m.value as mi, w.value as wi,  m.value - w.value as d
from countrytostat as m inner join
(select * from countrytostat where statId = 20) as w on m.CountryId = w.CountryId 
where m.statId = 21 order by (m.value - w.value) desc limit 1) as r on c.CountryId = r.CountryId;

# Q4
select avg(countrytostat.value) from countrytostat where StatId = 52;

# Q5
select avg(countrytostat.value) from countrytostat where StatId = 4;

# Q6
select avg(countrytostat.value) from countrytostat where StatId = 61;

# Q7
select avg(countrytostat.value) from countrytostat where StatId = 65;

# Q8
select GroupName, count(country.GroupId) from country, hdi.groups 
where hdi.groups.GroupId = country.GroupId group by country.GroupId;

# Q9
select distinct (select sum(countrytostat.value) from countrytostat where StatId = 27) - (select sum(countrytostat.value) from countrytostat where StatId = 26) as "Total population difference" from countrytostat;

# Q10
select v.CountryName, countrytostat.value as "HDI Rank" from countrytostat, country as v inner join
(select * from countrytostat where statId = 75 order by countrytostat.value desc limit 5) as v1 on v.CountryId = v1.CountryId 
where v1.CountryId = countrytostat.CountryId and countrytostat.StatID = 1;

# View
select * from ui_view;
select `HDI Rank` from ui_view;

# Store procs
# IN
select CountryName from country;
CALL getCountryStats("Turkey");

# OUT
CALL MostGNIDifference(@mCountry);
select @mCountry as "The most GNI differenece between men and women in a country";

# INOUT
select RegionName from region;
set @wCountry = "Arab States";
CALL worstCountryOfRegion(@wCountry);
select @wCountry as "The worst country of the region";


